# ArgoCD Deployment

This repository contains the configuration files and resources for deploying applications using ArgoCD and Kustomize.

## Prerequisites

- [ArgoCD](https://argoproj.github.io/argo-cd/getting_started/) - The GitOps continuous delivery tool for Kubernetes.
- [Kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/) - A Kubernetes native configuration management tool.

## Deploying

```sh
kubectl apply -k environments/prod
```

## Repository Structure

The repository is organized as follows:

```sh
├── bases
│   └── kustomization.yaml
└── environments
    └── prod
        ├── argocd-server.cert.yaml
        ├── argocd-server.ing.yaml
        ├── kustomization.yaml
        ├── patches
        │   ├── argocd-cmd-params.cm.yaml
        │   ├── argocd-notifications-controller.dp.yaml
        │   ├── argocd-rbac.cm.yaml
        │   ├── argocd-server.svc.yaml
        │   └── argocd.cm.yaml
        └── secrets
            ├── gitlab-sso.sealedsecret.yaml
            ├── gitlab-sso.secret.yaml
            └── kustomization.yaml
```
